class parent{
    void add(int a){
        System.out.println("method with one parameter a+2="+(a+2));
    }
 
 
 }
 class child extends parent{
    void add(int a, int b,int c){
        System.out.println("method with three parameters a+b+c = "+(a+b+c));
    }
 }
 class methodOLInheri{
    public static void main(String[] args){
        child obj = new child();
        obj.add(10);
        obj.add(10,20,30);
    }
}
 