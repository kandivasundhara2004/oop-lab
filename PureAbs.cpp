#include<iostream>
using namespace std;
class pureAbstract {
   public:
   virtual void examcell()=0;
   void collegeFinance();


};
class Derived: public pureAbstract{
   public:
   void examcell(){
       cout<<"Inside the admin block\n";
   }
   void collegeFinance(){
       cout<<"In the derived class\n";
   }
};
int main(){
   Derived obj;
   obj.collegeFinance();
   obj.examcell();
}
