class Student{
    String fName;
    int rNum;
    double semPercent;
    String cName;
    int cCode;
    Student(String fullName,int rollNum, double semPercentage, String collegeName, int collegeCode){
        fName = fullName;
        rNum = rollNum;
        semPercent = semPercentage;
        cName = collegeName;
        cCode = collegeCode;
    }
    void displayMsg(){
        System.out.println(fName);
        System.out.println(rNum);
        System.out.println(semPercent);
        System.out.println(cName);
        System.out.println(cCode);
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String args[]){
        Student obj = new Student("vasu",77,8.75,"MVGR",22);
        obj.displayMsg();
        obj = null;
        System.gc();
    }
}