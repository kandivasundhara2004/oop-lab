import java.applet.Applet;
import java.awt.Graphics;

class AppletDemo extends Applet {
    public void paint(Graphics g) {
        g.drawString("welcome", 150, 150);
    }
}
/*
 * <html>
 * <applet code="AppletDemo",width=200,height=60>
 * </applet>
 * </html>
 */