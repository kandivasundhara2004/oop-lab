import java.util.*;
import java.lang.*;
class Students{
    String fname;
    double semPercent;
    String collegeName;
    int collegeCode;
    Students(){
        collegeName = "MVGR";
        collegeCode = 22;
        System.out.println("The collegeName and collegeCode is: ");
    }
    Students(String fullName, double semPercentage){
        this.fname = fullName;
        this.semPercent = semPercentage;
    }
    protected void finalize(){
        System.out.println("Dead");
    }
    public static void main(String args[]){
        Students obj1 = new Students();
        System.out.println(obj1.collegeName);
        System.out.println(obj1.collegeCode);
        Students  obj2 = new Students("Vasu", 8.75);
        System.out.println("Name: "+ obj2.fname + "Percent: " + obj2.semPercent);
        obj1=null;
        System.gc();
    }
}