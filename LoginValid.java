import java.awt.Frame;
import java.awt.event.ActionListener;
import java.awt.event.*;
import java.awt.*;

class LoginValid {
    Label userLabel, passwordLabel;
    TextField userText, passText, status;
    Button submit;

    LoginValid() {
        Frame f = new Frame("Login Form");
        f.setSize(400, 400);
        f.setVisible(true);
        f.setResizable(false);
        f.setLayout(null);
        userLabel = new Label("User name:");
        userLabel.setBackground(Color.gray);
        userLabel.setBounds(20, 50, 60, 20);
        userText = new TextField(20);
        userText.setBounds(100, 50, 60, 20);
        passwordLabel = new Label("Password:");
        passwordLabel.setBackground(Color.gray);
        passwordLabel.setBounds(20, 100, 60, 20);
        passText = new TextField(20);
        passText.setBounds(100, 100, 60, 20);
        passText.setEchoChar('*');
        submit = new Button("Submit");
        submit.setBackground(Color.gray);
        submit.setBounds(50, 130, 50, 20);
        f.add(userLabel);
        f.add(passwordLabel);
        f.add(userText);
        f.add(passText);
        f.add(submit);
        submit.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Success objs = new Success();
                        if (userText.getText().equals("Student") && passText.getText().equals("1234")) {
                            objs.statusCondition.setText("Login successful");
                        } else {
                            objs.statusCondition.setText("Login failure ..please try again");
                        }
                    }
                });
        f.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        f.dispose();
                    }
                });
    }

    public static void main(String[] args) {
        LoginValid obj = new LoginValid();
    }
}

class Success {
    TextField statusCondition;

    Success() {
        Frame fs = new Frame("Msg Promt");
        fs.setSize(200, 200);
        fs.setVisible(true);
        fs.setResizable(false);
        fs.setLayout(null);
        statusCondition = new TextField(40);
        statusCondition.setBackground(Color.gray);
        statusCondition.setBounds(40, 80, 100, 30);
        fs.add(statusCondition);
        fs.addWindowListener(
                new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        fs.dispose();
                    }
                });
    }
}