class parent{
    void add(int a, int b){
        System.out.println("Sum of int a+b: "+(a+b));
    }
    void add(float a, float b){
        System.out.println("sum of floats a+b : "+(a+b));
    }
    void add(double a, double b ){
        System.out.println("Sum of doubles a+b : "+(a+b));
    }
 }
 class child extends parent{
    void add(String a, String b){
        System.out.println("concatentnaton of a+b : "+(a+b));
    }
 }
 class overriding{
    public static void main(String[] args){
        child obj = new child();
        obj.add(2,3);
        obj.add(5.3f,3.4f);
        obj.add(2.6,1.2);
        obj.add(4.2,0.4);
        obj.add(2.3f,6);
    }
 }
 
 
 
 