import java.lang.*;
class Threads implements Runnable{
 
    int num;
    Threads(int num){
        this.num=num;
    }
    public void run(){
        multip(num);
    }
    void multip(int n){
        for (int i=0;i<=5;i++){
            System.out.println(n+" X "+i+" = "+(n*i));
        }
    }
}
class RunInterface {
    public static void main(String[] args) {
        Threads t1=new Threads(3);
        Threads t2=new Threads(5);
        t1.run();
        t2.run();
    }
}
