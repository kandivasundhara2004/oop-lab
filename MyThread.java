import java.lang.*;
class Threads extends Thread{
    int num;
    Threads(int num){
        this.num=num;
    }
    public void run(){
        multip(num);
    }
    void multip(int n){
        for (int i=0;i<=5;i++){
        System.out.println(n+" X "+i+" = "+(n*i));
        }
    }
}
class MyThread {
    public static void main(String[] args) {
        Threads t1=new Threads(2);
        Threads t2=new Threads(4);
        t1.start();
        t2.start();
    }
}
 