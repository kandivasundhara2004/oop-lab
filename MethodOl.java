class Sample{
    private void add(int a){
        System.out.println("method with one parameter a+2 = "+(a+2));
    }
    protected void add(int a, int b){
        System.out.println("method with two parameter a+b="+(a+b));
        add(10);
    }
    public void add(int a, int b,int c){
        System.out.println("method with three parameter a+b+c= "+(a+b+c));
    }
 }
class methodOL{
    public static void main (String[] args){
        Sample obj = new Sample();
        //obj.add(10);
        obj.add(10,20);
        obj.add(10,20,30);
    }
 }
 