import java.lang.*;
class MutableImmutable {
    public static void main(String[] args) {
        int a=20;
        String s="Vasu";
        float f=4.3f;
        StringBuffer str=new StringBuffer("good");
        StringBuilder strBuild=new StringBuilder("MVGR ");
        Boolean bool=false;
        System.out.println("int before : "+System.identityHashCode(a));
        a=a+1;
        System.out.println("int after : "+System.identityHashCode(a));
        System.out.println("boolean before :"+System.identityHashCode(bool));
        bool=true;
        System.out.println("boolean after :"+System.identityHashCode(bool));
        System.out.println("String before :"+System.identityHashCode(s));
        s=s+"jetti";
        System.out.println("string after :"+System.identityHashCode(s));
        System.out.println("float before :"+System.identityHashCode(f));
        f=f+1;
        System.out.println("float after : "+System.identityHashCode(f));
        System.out.println("String buffer before :"+System.identityHashCode(str));
        str.append("morning");
        System.out.println("String beffer after :"+System.identityHashCode(str));
        System.out.println("String builder before :"+System.identityHashCode(strBuild));
        strBuild.append("College");
        System.out.println("String builder after :"+System.identityHashCode(strBuild));
    }
}
